package lv.hansagames.chaining.base 
{
	import lv.hansagames.chaining.core.AbstractCommand;
	import lv.hansagames.chaining.event.CommandEvent;
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public class ExecuteMethodCommand extends AbstractCommand 
	{
		private var func:Function;
		private var params:Array;
		
		public function ExecuteMethodCommand(id:String,func:Function,params:Array=null) 
		{
			super(id);
			this.func = func;
			this.params=params
		}
		
		override protected function execute():void 
		{
			try
			{
				
				func.apply(null, params);
				complete();
			}
			catch (err:Error)
			{
				trace("EXECUTE COMMAND ERROR: "+err);
				stop();
				dispatcher.dispatch(CommandEvent.FAILED,this,err);
			}
			
			
		}
		
		
	}

}