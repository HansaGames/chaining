package lv.hansagames.chaining.core 
{
	import org.osflash.signals.Signal;
	
	 /**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public interface ICommand
	{
		function get commandId():String;
		function set commandId(value:String):void;
		function get dispatcher():Signal;
		
		function get autoDestroy():Boolean 
		function set autoDestroy(value:Boolean):void 
		
		function start():void;
		function stop():void;
		
		function addProperty(id:String, data:*):ICommand;
		
		function destroy():void;
	}

}