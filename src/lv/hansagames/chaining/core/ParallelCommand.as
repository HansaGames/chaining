package lv.hansagames.chaining.core 
{
	import lv.hansagames.chaining.event.CommandEvent;
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public class ParallelCommand extends AbstractListCommand 
	{	
		public function ParallelCommand(id:String,commands:Array) 
		{
			super(id,commands);
		}
		
		override protected function execute():void 
		{
			for each (var item:AbstractCommand in commands) 
			{
				if(item && item.dispatcher)
				{
					item.dispatcher.add(onSubCommandSignal);
					item.start();
				}
			}
		}
		
		override protected function onSubCommandSignal(id:String,target:*=null,data:*=null):void 
		{
			if (id == CommandEvent.FINISHED)
			{
				if(target && target.dispatcher)
					ICommand(target).dispatcher.removeAll();
				finishedCommands++;
				
				if (finishedCommands >= totalCommands)
					complete();
			}
			else if (id == CommandEvent.FAILED)
			{
				error();
			}
		}
	}

}