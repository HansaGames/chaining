package lv.hansagames.chaining.core
{
	import lv.hansagames.chaining.event.CommandEvent;
	
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public class SequenceCommand extends AbstractListCommand
	{
		
		public function SequenceCommand(id:String, commands:Array=null)
		{
			super(id, commands);
		}
		
		override protected function execute():void
		{
			var item:AbstractCommand;
			
			if (totalCommands > 0)
			{
				finishedCommands = 0;
				item = commands[finishedCommands];
				if (item && item.dispatcher)
				{
					item.dispatcher.add(onSubCommandSignal);
					item.start();
				}
			}
			else
			{
				complete();
			}
		}
		
		override protected function onSubCommandSignal(id:String, target:* = null, data:* = null):void
		{
			var item:AbstractCommand;
			if (id == CommandEvent.FINISHED)
			{
				if (target && target.dispatcher)
					ICommand(target).dispatcher.removeAll();
				finishedCommands++;
				
				if (finishedCommands >= totalCommands)
				{
					isRunning = false;
					if(dispatcher)
						dispatcher.dispatch(CommandEvent.FINISHED, this);
				}
				else
				{
					item = commands[finishedCommands];
					
					if (item && item.dispatcher)
					{
						item.dispatcher.add(onSubCommandSignal);
						item.start();
					}
				}
			}
			else if (id == CommandEvent.FAILED)
			{
				error();
			}
		}
	}

}