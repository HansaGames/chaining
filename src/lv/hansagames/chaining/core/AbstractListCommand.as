package lv.hansagames.chaining.core
{
	
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public class AbstractListCommand extends AbstractCommand
	{
		protected var commands:Array;
		protected var finishedCommands:int;
		protected var totalCommands:int;
		
		public function AbstractListCommand(id:String, commands:Array = null)
		{
			var length:int
			super(id);
			this.commands = commands;
			if (commands)
			{
				length = commands.length;
				for (var i:int = 0; i < length; i++)
				{
					if (commands[i])
						totalCommands++;
				}
			}
		}
		
		protected function onSubCommandSignal(id:String, caller:* = null, data:* = null):void
		{
		
		}
		
		public function addCommand(command:ICommand):void
		{
			if (!commands)
				commands = [];
			commands.push(command);
			totalCommands++;
		}
		
		override public function stop():void
		{
			super.stop();
			
			if (commands)
			{
				for each (var item:AbstractCommand in commands)
				{
					if(item)
						item.stop();
				}
			}
		}
		
		override public function destroy():void
		{
			super.destroy();
			
			if (commands)
			{
				for each (var item:AbstractCommand in commands)
				{
					if(item)
						item.destroy();
				}
				
				finishedCommands = 0;
				totalCommands = 0;
				
				commands.length = 0;
				commands = null;
			}
		}
	
	}

}