package lv.hansagames.chaining.core
{
	import lv.hansagames.chaining.event.CommandEvent;
	import lv.hansagames.chaining.vo.CommandDataVO;
	import org.osflash.signals.Signal;
	
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public class AbstractCommand implements ICommand
	{
		protected var data:CommandDataVO;
		protected var _commandId:String;
		
		protected var _dispatcher:Signal;
		
		protected var isRunning:Boolean;
		
		protected var _autoDestroy:Boolean;
		
		public function AbstractCommand(id:String, data:CommandDataVO = null)
		{
			this.data = data;
			_commandId = id;
			_dispatcher = new Signal();
		}
		
		protected function execute():void
		{
		
		}
		
		protected function complete():void
		{
			isRunning = false;
			if (dispatcher)
				dispatcher.dispatch(CommandEvent.FINISHED, this);
		}
		
		protected function error():void
		{
			stop();
			if (dispatcher)
				dispatcher.dispatch(CommandEvent.FAILED, this);
			if (autoDestroy)
				destroy();
		}
		
		/* INTERFACE lv.hansagames.chaining.core.ICommand */
		
		public function get commandId():String
		{
			return _commandId;
		}
		
		public function set commandId(value:String):void
		{
			_commandId = value;
		}
		
		public function get dispatcher():Signal
		{
			return _dispatcher;
		}
		
		public function get autoDestroy():Boolean
		{
			return _autoDestroy;
		}
		
		public function set autoDestroy(value:Boolean):void
		{
			_autoDestroy = value;
		}
		
		public function start():void
		{
			if (!isRunning)
			{
				isRunning = true;
				if (dispatcher)
					dispatcher.dispatch(CommandEvent.STARTED, this);
				execute();
			}
		
		}
		
		public function stop():void
		{
			isRunning = false;
		}
		
		public function destroy():void
		{
			stop();
			isRunning = false;
			_autoDestroy = false;
			_commandId = '';
			if (_dispatcher)
			{
				_dispatcher.removeAll();
				_dispatcher = null;
			}
			
			if (data)
			{
				data.destroy();
				data = null;
			}
		}
		
		public function addProperty(id:String, data:*):ICommand
		{
			if (!this.data)
				this.data = new CommandDataVO()
			this.data.addProperty(id, data);
			return this;
		}
	
	}

}