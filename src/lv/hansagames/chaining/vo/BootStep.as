package lv.hansagames.chaining.vo 
{
	import lv.hansagames.chaining.core.ICommand;
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public final class BootStep 
	{
		private var _id:String;
		private var _command:ICommand;
		private var _pauseAfter:Boolean;
		private var _pauseBefore:Boolean;
		
		public function BootStep() 
		{
			
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function set id(value:String):void 
		{
			_id = value;
		}
		
		public function get command():ICommand 
		{
			return _command;
		}
		
		public function set command(value:ICommand):void 
		{
			_command = value;
		}
		
		public function get pauseAfter():Boolean 
		{
			return _pauseAfter;
		}
		
		public function set pauseAfter(value:Boolean):void 
		{
			_pauseAfter = value;
		}
		
		public function get pauseBefore():Boolean 
		{
			return _pauseBefore;
		}
		
		public function set pauseBefore(value:Boolean):void 
		{
			_pauseBefore = value;
		}
		
		public function destroy():void
		{
			if (command)
			{
				command.destroy();
				command = null;
			}
		}
		
	}

}