package lv.hansagames.chaining.vo
{
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public final class CommandDataVO
	{
		private var properties:Dictionary
		
		public function CommandDataVO()
		{
		
		}
		
		public function addProperty(id:String, data:*):CommandDataVO
		{
			if (!properties)
				properties = new Dictionary(true);
			
			properties[id] = data;
			return this;
		}
		
		public function getPropertyByID(id:String):*
		{
			if (properties)
				return properties[id];
			return null;
		}
		
		public function removePropertyByID(id:String):CommandDataVO
		{
			if (properties && properties[id])
				delete properties[id];
			return this;
		}
		
		public function destroy():void
		{
			if (properties)
			{
				for (var name:String in properties)
				{
					delete properties[name];
				}
				properties = null;
			}
		}
	
	}

}