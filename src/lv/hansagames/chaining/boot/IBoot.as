package lv.hansagames.chaining.boot 
{
	import lv.hansagames.chaining.core.ICommand;
	import org.osflash.signals.Signal;
	
	 /**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public interface IBoot 
	{
		function get dispatcher():Signal;
		
		function get running():Boolean;
		
		function start():void;
		function stop():void;
		function destroy():void;
		function pause():void;
		function resume():void;
		
		function get autoDestroy():Boolean 
		function set autoDestroy(value:Boolean):void 
		
		function get currentStep():String;
		function set currentStep(value:String):void;
		
		function set stepFactory(value:Function):void;
		
		function addPauseBefore(position:String):IBoot;
		function addPauseAfter(position:String):IBoot;
		function removePauseBefore(position:String):IBoot;
		function removePauseAfter(position:String):IBoot;
		function removeAllPauses():void;
		
		function setStep(position:String, step:ICommand):IBoot;
		
		function addStepAfter(position:String, placeID:String):IBoot;
		function addStepBefore(position:String, placeID:String):IBoot;
		function addStepAtBegining(placeID:String):IBoot;
		function addStepAtEnd(placeID:String):IBoot;
		
		function getCommandByID(id:String):ICommand;
		
		function removeStep(id:String):IBoot;
		function removeAllSteps():void;
	}

}