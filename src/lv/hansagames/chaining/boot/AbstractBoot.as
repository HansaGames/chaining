package lv.hansagames.chaining.boot
{
	import lv.hansagames.chaining.core.ICommand;
	import lv.hansagames.chaining.event.BootEvent;
	import lv.hansagames.chaining.event.CommandEvent;
	import lv.hansagames.chaining.vo.BootStep;
	import lv.hansagames.datatype.core.IIterator;
	import lv.hansagames.datatype.dlinkedlist.DoubleLinkedList;
	import lv.hansagames.datatype.dlinkedlist.DoubleLinkedListNode;
	import lv.hansagames.datatype.dlinkedlist.IDoubleLinkedList;
	import lv.hansagames.datatype.dlinkedlist.IDoubleLinkedLIstNode;
	import org.osflash.signals.Signal;
	
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public class AbstractBoot implements IBoot
	{
		private var _dispatcher:Signal;
		
		private var stepList:DoubleLinkedList;
		
		private var _running:Boolean;
		private var _autoDestroy:Boolean;
		private var paused:Boolean;
		
		private var previousStepItem:BootStep;
		private var currentStepItem:BootStep;
		
		private var _stepFactory:Function;
		
		public function AbstractBoot()
		{
			stepList = new DoubleLinkedList();
			_dispatcher = new Signal();
		}
		
		public function get dispatcher():Signal
		{
			return _dispatcher;
		}
		
		public function start():void
		{
			if (!running)
			{
				_running = true;
				
				if (!stepList.length == 0)
				{
					if (!stepList.currentPointer)
						stepList.currentPointer = stepList.head;
					currentStepItem = stepList.currentPointer.data;
					
					paused = currentStepItem.pauseBefore;
					
					if (!paused)
					{
						executeStep(currentStepItem.command);
					}
				}
				else
				{
					if (autoDestroy)
						destroy();
					dispatcher.dispatch(BootEvent.COMPLETE, this);
				}
			}
		}
		
		private function executeStep(step:ICommand):void
		{
			if (!paused)
			{
				if (step)
				{
					step.dispatcher.add(onLocalStep);
					step.start();
				}
			}
		}
		
		private function onLocalStep(id:String, target:ICommand = null, data:* = null):void
		{
			if (id == CommandEvent.FINISHED)
			{
				target.dispatcher.remove(onLocalStep);
				dispatcher.dispatch(BootEvent.STEP_COMPLETE, this, currentStep);
				if (stepList.currentPointer.next)
				{
					stepList.currentPointer = stepList.currentPointer.next;
					previousStepItem = currentStepItem;
					currentStepItem = stepList.currentPointer.data;
					paused = previousStepItem.pauseAfter;
					
					if (autoDestroy)
					{
						removeStep(previousStepItem.id);
						target.destroy();
					}
					
					if (!paused)
					{
						executeNextCommand();
					}
				}
				else
				{
					_running = false;
					
					if (autoDestroy)
					{
						removeStep(currentStep);
						target.destroy();
						destroy();
					}
					
					dispatcher.dispatch(BootEvent.COMPLETE, this);
				}
				
			}
			else if (id == CommandEvent.FAILED)
			{
				_running = false;
				target.dispatcher.remove(onLocalStep);
				
				if (autoDestroy)
				{
					target.destroy();
					destroy();
				}
				
				dispatcher.dispatch(BootEvent.FAILED, this, currentStep);
			}
		}
		
		public function addStepAfter(position:String, placeID:String):IBoot
		{
			var node:DoubleLinkedListNode = getStepNodeByID(position);
			var step:BootStep;
			var newNode:DoubleLinkedListNode
			
			if (node)
			{
				newNode = getStepNodeByID(placeID);
				
				if (!newNode)
				{
					step = new BootStep();
					step.id = placeID;
					
					newNode = new DoubleLinkedListNode()
					newNode.id = placeID;
					newNode.data = step;
				}
				else
				{
					stepList.removeNode(newNode);
				}
				
				stepList.addNodeAfter(newNode, node);
			}
			return this;
		}
		
		private function getStepNodeByID(id:String):DoubleLinkedListNode
		{
			var itterator:IIterator = stepList.iterrator;
			var node:DoubleLinkedListNode
			
			while (itterator.hasNext())
			{
				node = itterator.next();
				if (node && node.id == id)
					return node;
			}
			return null;
		}
		
		private function getStepByID(id:String):BootStep
		{
			var itterator:IIterator = stepList.iterrator;
			var node:DoubleLinkedListNode
			
			while (itterator.hasNext())
			{
				node = itterator.next();
				if (node && node.id == id)
					return node.data;
			}
			return null;
		}
		
		public function addStepBefore(position:String, placeID:String):IBoot
		{
			var node:DoubleLinkedListNode = getStepNodeByID(position);
			var newNode:DoubleLinkedListNode
			var step:BootStep;
			
			if (node)
			{
				newNode = getStepNodeByID(placeID);
				
				if (!newNode)
				{
					
					step = new BootStep();
					step.id = placeID;
					
					newNode = new DoubleLinkedListNode()
					newNode.id = placeID;
					newNode.data = step;
				}
				else
				{
					stepList.removeNode(newNode);
				}
				
				stepList.addNodeBefore(newNode, node);
			}
			return this;
		}
		
		public function addStepAtBegining(placeID:String):IBoot
		{
			var newNode:DoubleLinkedListNode = getStepNodeByID(placeID);
			var step:BootStep;
			
			if (!newNode)
			{
				step = new BootStep();
				step.id = placeID;
				
				newNode = new DoubleLinkedListNode()
				newNode.id = placeID;
				newNode.data = step;
			}
			else
			{
				stepList.removeNode(newNode);
			}
			
			stepList.addNodeAtBegining(newNode);
			return this;
		}
		
		public function addStepAtEnd(placeID:String):IBoot
		{
			var newNode:DoubleLinkedListNode = getStepNodeByID(placeID);
			var step:BootStep;
			
			if (!newNode)
			{
				step = new BootStep();
				step.id = placeID;
				
				newNode = new DoubleLinkedListNode()
				newNode.id = placeID;
				newNode.data = step;
			}
			else
			{
				stepList.removeNode(newNode);
			}
			
			stepList.addNodeAtEnd(newNode);
			return this;
		}
		
		public function getCommandByID(id:String):ICommand
		{
			var node:DoubleLinkedListNode = getStepNodeByID(id);
			
			if (node)
				return node.data.command;
			return null;
		}
		
		public function removeStep(id:String):IBoot
		{
			var node:DoubleLinkedListNode = getStepNodeByID(id)
			
			if (node)
				stepList.removeNode(node);
			return this;
		}
		
		public function get running():Boolean
		{
			return _running;
		}
		
		public function get autoDestroy():Boolean
		{
			return _autoDestroy;
		}
		
		public function set autoDestroy(value:Boolean):void
		{
			_autoDestroy = value;
		}
		
		public function get currentStep():String
		{
			if (currentStepItem)
				return currentStepItem.id;
			return '';
		}
		
		public function set currentStep(value:String):void
		{
			var node:DoubleLinkedListNode = getStepNodeByID(value);
			
			if (node)
				stepList.currentPointer = node;
		}
		
		public function stop():void
		{
			if (running)
			{
				_running = false;
			}
		}
		
		public function destroy():void
		{
			if (dispatcher)
			{
				_dispatcher.removeAll();
				_dispatcher = null;
			}
			
			if (stepList)
			{
				destroyAllCommands(stepList);
				stepList.removeAll();
				stepList = null;
			}
			
			_running = false;
			_autoDestroy = false;
			previousStepItem = null;
			currentStepItem = null;
			paused = false;
		}
		
		private function destroyAllCommands(source:IDoubleLinkedList):void
		{
			var itterator:IIterator;
			var node:IDoubleLinkedLIstNode;
			var step:BootStep;
			if (source)
			{
				itterator = source.iterrator;
				
				if (itterator)
				{
					while (itterator.hasNext())
					{
						node = itterator.next();
						if (node)
						{
							step = node.data;
							
							if (step)
								step.destroy();
						}
					}
				}
			}
		}
		
		public function pause():void
		{
			if (running && !paused)
			{
				paused = true;
			}
		}
		
		public function resume():void
		{
			if (paused && running)
			{
				paused = false;
				executeStep(stepList.currentPointer.data);
			}
		}
		
		public function addPauseBefore(position:String):IBoot
		{
			var step:BootStep = getStepByID(position);
			
			if (step)
				step.pauseBefore = true;
			
			return this;
		}
		
		public function addPauseAfter(position:String):IBoot
		{
			var step:BootStep = getStepByID(position);
			
			if (step)
				step.pauseAfter = true;
			
			return this;
		}
		
		public function removePauseBefore(position:String):IBoot
		{
			var step:BootStep = getStepByID(position);
			
			if (step)
				step.pauseBefore = false;
			return this
		}
		
		public function removePauseAfter(position:String):IBoot
		{
			var step:BootStep = getStepByID(position);
			
			if (step)
				step.pauseAfter = false;
			return this
		}
		
		public function removeAllPauses():void
		{
			var itterator:IIterator;
			var node:IDoubleLinkedLIstNode;
			var step:BootStep;
			if (stepList)
			{
				itterator = stepList.iterrator;
				
				if (itterator)
				{
					while (itterator.hasNext())
					{
						node = itterator.next();
						if (node)
						{
							step = node.data;
							
							if (step)
							{
								step.pauseAfter = false;
								step.pauseBefore = false;
							}
						}
					}
				}
			}
		}
		
		public function removeAllSteps():void
		{
			if (stepList)
				stepList.removeAll();
		}
		
		public function set stepFactory(value:Function):void
		{
			if (!running)
				_stepFactory = value;
		}
		
		public function setStep(position:String, step:ICommand):IBoot
		{
			var node:DoubleLinkedListNode = getStepNodeByID(position);
			
			if (node)
			{
				if (node.data)
				{
					destroyCommand(node.data.command);
				}
				
				node.data.command = step;
			}
			return this;
		}
		
		private function destroyCommand(command:ICommand):void
		{
			if (command)
				command.destroy();
		}
		
		private function executeNextCommand():void
		{
			var nextCommand:ICommand;
			
			while (!paused && !nextCommand && currentStepItem)
			{
				previousStepItem = currentStepItem;
				currentStepItem = stepList.currentPointer.data;
				paused = currentStepItem.pauseBefore;
				
				if (_stepFactory !== null)
				{
					nextCommand = _stepFactory(currentStepItem.id);
				}
				
				if (!nextCommand)
				{
					nextCommand = currentStepItem.command;
				}
				
				if (!nextCommand)
				{
					stepList.currentPointer = stepList.currentPointer.next;
					paused = currentStepItem.pauseAfter;
					dispatcher.dispatch(BootEvent.STEP_COMPLETE, this, previousStepItem.id);
					currentStepItem = stepList.currentPointer.next.data;
				}
			}
			
			if (!paused)
			{
				if (nextCommand)
				{
					executeStep(nextCommand);
				}
				else
				{
					if (autoDestroy)
						destroy();
					dispatcher.dispatch(BootEvent.COMPLETE, this);
				}
			}
		}
	
	}

}