package lv.hansagames.chaining.event 
{
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public final class CommandEvent 
	{		
		public static const STARTED:String = "CommandEvent.STARTED";
		public static const FINISHED:String = "CommandEvent.FINISHED";
		public static const FAILED:String = "CommandEvent.FAILED";
	}

}