package lv.hansagames.chaining.event 
{
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public final class BootEvent 
	{
		public static const COMPLETE:String = "BootEvent.COMPLETE";
		public static const STEP_COMPLETE:String = "BootEvent.STEP_COMPLETE";
		public static const FAILED:String = "BootEvent.FAILED";
		public static const GO_TO_STEP:String = "BootEvent.GO_TO_STEP";
	}

}