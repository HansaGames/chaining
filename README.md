##AS3 implementation of Command pattern

This library offers base functionality to use command pattern in as3 programming language.
If you need more information on Command pattern and how to use it, try these great tutorials that inspired creation of this library:

- [Thinking in commands part 1](http://code.tutsplus.com/tutorials/thinking-in-commands-part-1-of-2--active-3383)
- [Thinking in commands part 2](http://code.tutsplus.com/tutorials/thinking-in-commands-part-2-of-2--active-3537)
- [Loading data with commands](http://code.tutsplus.com/tutorials/loading-data-with-commands--active-3900)

Addition to commands, this library introduces special kind of command called **"Boot"**. Basically it wrapper for **"Sequence list command"**, that excites all sub commands one after another.
But additional add possibility to pause boot process and organize commands, allowing to customize boot process for application. 

##Examples



###Basic commands
Basic command need to extend **Abstract Command** Class from library.
It contains base properties and methods, that are required for all commands.

Most methods are self-explanatory therefore I will explain only critical ones.

####execute
This method is heart of command. This method will be executed when command will be started.
That is place where you should add your business logic to method. in order to do it just override method and add 
your logic there: 

    override protected function execute():void 
    {
    		super.execute();
    			
    		if (1 == 1)
    		{
    			complete();
    		}
    		else
    		{
    			error();
    		}
    }
    	
That is minimalistic example how your command could look like. Basically you do your logic and at the end call one of the two methods **"complete"** when command was executed successfully or **"error"** when it failed.

####add Property
If your command need specific objects from outside, you can add them either in constructor, using **"CommandDataVO"**, or using commands method **"addProperty"**. This method returns command so you can chain it , if you need to add more properties. In order to retrieve these properties just use **"data.getPropertyByID"** method.

####auto Destroy
Normally, if your commands content allows that, command can be executed many times, before it is destroyed. By setting property **"autoDestroy" ** to true it will be destroyed and cleaned from memory after first execution.

####destroy
In these method you need to clean all properties and objects that you introduced in command. To do that , just override it and clean your objects.

###Command Chaining
To execute more as one command you need to use one of the list commands. To add commands to the list just use method **"addCommand"**.
####ParallelCommand
This command executes all child commands at the same time.
####SequenceCommand
This command executes child commands one after another.

###Boot
Boot is special kind of command that allows to execute commands in sequence, but with extra functions , like pause.

Adding method to boot is 2 steps process. At first you have to register command using one of 4 addStep methods. After that you can add commands to step using setStep method.

##Dependencies
- [As3 signals](https://github.com/robertpenner/as3-signals)
- [Data Types](https://bitbucket.org/HansaGames/datatypes)