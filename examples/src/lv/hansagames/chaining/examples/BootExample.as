package lv.hansagames.chaining.examples 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import lv.hansagames.chaining.base.ExecuteMethodCommand;
	import lv.hansagames.chaining.boot.AbstractBoot;
	import lv.hansagames.chaining.boot.IBoot;
	import lv.hansagames.chaining.examples.commands.CalculateSumCommand;
	import lv.hansagames.chaining.examples.commands.SayHiToCommand;
	import lv.hansagames.chaining.vo.CommandDataVO;
	
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public class BootExample extends Sprite 
	{
		private var boot:IBoot
		
		public function BootExample() 
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function onAdded(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			boot = new AbstractBoot();
			boot.dispatcher.add(onBoot);
			
			boot.addStepAtEnd("hi");
			boot.addStepAtEnd("sum");
			boot.addStepAtEnd("execute");
			
			boot.setStep("hi", new SayHiToCommand("hi", new CommandDataVO().addProperty("persons_name", "Uldis")));
			boot.setStep("execute", new ExecuteMethodCommand("execute_command",testFunction));
			boot.setStep("sum", new CalculateSumCommand("sum", new CommandDataVO().addProperty("n1", 1).addProperty("n2", 2)));
			
			boot.start();
		}
		
		private function onBoot(id:String,target:*=null,data:*=null):void 
		{
			trace(id + " :: " + data);
		}
		
		private function testFunction():void 
		{
			trace("FUNCTION executed");
		}
		
	}

}