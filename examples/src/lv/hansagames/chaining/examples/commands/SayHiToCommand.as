package lv.hansagames.chaining.examples.commands
{
	import lv.hansagames.chaining.core.AbstractCommand;
	import lv.hansagames.chaining.vo.CommandDataVO;
	
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public class SayHiToCommand extends AbstractCommand 
	{
		private var personsName:String
		
		public function SayHiToCommand(id:String, data:CommandDataVO=null) 
		{
			super(id, data);
			
		}
		
		override protected function execute():void 
		{
			super.execute();
			
			if (data)
			{
				personsName = data.getPropertyByID("persons_name");
				trace("Hi " + personsName + " !");
				complete();
			}
			else
			{
				error();
			}
			
			
		}
		
		override public function destroy():void 
		{
			super.destroy();
			personsName = '';
		}
		
	}

}