package lv.hansagames.chaining.examples.commands
{
	import lv.hansagames.chaining.core.AbstractCommand;
	import lv.hansagames.chaining.vo.CommandDataVO;
	
	/**
	 * ...
	 * @author Uldis Baurovskis
	 */
	public class CalculateSumCommand extends AbstractCommand 
	{
		private var n1:Number;
		private var n2:Number;
		
		public function CalculateSumCommand(id:String, data:CommandDataVO=null) 
		{
			super(id, data);
			
		}
		
		override protected function execute():void 
		{
			super.execute();
			
			if (data)
			{
				n1 = Number(data.getPropertyByID("n1"));
				n2 = Number(data.getPropertyByID("n2"));
				trace("SUM " + (n1 + n2));
				complete();
			}
			else
			{
				error();
			}
		}
		
	}

}